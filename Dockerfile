FROM elixir:1.9-slim

RUN /usr/local/bin/mix local.hex --force && \
/usr/local/bin/mix local.rebar --force

ENV APP_HOME /substats_app

RUN mkdir $APP_HOME

ARG PG_HOST
ARG PG_PASS
ARG MIX_ENV
ARG SECRET_KEY_BASE

# copy mix.exs only
COPY mix.* $APP_HOME/
COPY apps/substats/mix.* $APP_HOME/apps/substats/
COPY apps/substats_data/mix.* $APP_HOME/apps/substats_data/
COPY apps/substats_influxdb/mix.* $APP_HOME/apps/substats_influxdb/
COPY apps/substats_repository/mix.* $APP_HOME/apps/substats_repository/

WORKDIR $APP_HOME

RUN mix deps.get && mix compile

COPY config $APP_HOME/config
COPY apps $APP_HOME/apps