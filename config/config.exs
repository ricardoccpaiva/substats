# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :substats_web,
  ecto_repos: [Substats.Repo],
  generators: [context_app: false]

# Configures the endpoint
config :substats_web, Substats.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pyufcA0yaG7CrLSss8mk5UI08nWoblVYXnQKelvPGV1PiRyys6ya7PF87brEjcbR",
  render_errors: [view: Substats.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Substats.PubSub, adapter: Phoenix.PubSub.PG2]

config :substats,
  ecto_repos: [Substats.Repo],
  generators: [context_app: false]

# Configures the endpoint
config :substats, Substats.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Wk7KS+r+RZQQgx8pAinMYf+NYNsx5Mo7k1cFnClSMxeB/j88WWQ+4OqEc3MyViIy",
  render_errors: [view: Substats.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Substats.PubSub, adapter: Phoenix.PubSub.PG2]

# By default, the umbrella project as well as each child
# application will require this configuration file, ensuring
# they all use the same configuration. While one could
# configure all applications here, we prefer to delegate
# back to each application for organization purposes.
import_config "../apps/*/config/config.exs"

# Sample configuration (overrides the imported configuration above):
#
#     config :logger, :console,
#       level: :info,
#       format: "$date $time [$level] $metadata$message\n",
#       metadata: [:user_id]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
