use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :substats_web, Substats.Endpoint,
  http: [port: 4002],
  server: false

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :substats, Substats.Endpoint,
  http: [port: 4002],
  server: false
