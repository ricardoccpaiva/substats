defmodule SubstatsData.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  alias SubstatsData.Fetcher
  use Application

  def start(_type, _args) do
    setup_metro_lx_gateway_props()
    {interval, ""} = Integer.parse(System.get_env("REFRESH_INTERVAL"))
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: SubstatsData.Worker.start_link(arg)
      {Fetcher,
       %{
         refresh_interval: interval,
         fail_refresh_interval: 10000
       }},
      SubstatsInfluxDb.Connection
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SubstatsData.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp setup_metro_lx_gateway_props do
    Application.put_env(
      :substats_data,
      :metro_lx_api_auth_header,
      System.get_env("METRO_LX_API_AUTH_HEADER")
    )
  end
end
