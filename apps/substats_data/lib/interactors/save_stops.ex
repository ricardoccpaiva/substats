defmodule SubstatsData.Interactors.SaveStops do
  alias SubstatsRepository.Models
  alias SubstatsData.Gateways.MetroLx

  def save_all(access_token) do
    with {:ok, destinations} <- MetroLx.get_destination_info(access_token),
         {:ok, stops} <- MetroLx.get_stops_info(access_token) do
      Enum.map(stops, fn stop ->
        Models.Stop.changeset(%Models.Stop{}, build_props(stop, destinations))
        |> SubstatsRepository.Repo.insert_or_update()
      end)
    else
      :error ->
        :error
    end
  end

  defp build_props(stop, destinations) do
    destination =
      Enum.find(destinations, fn dst ->
        dst["nome_destino"] == stop["stop_name"]
      end)

    %{
      external_id: stop["stop_id"],
      internal_id: destination["id_destino"],
      name: stop["stop_name"],
      lat: stop["stop_lat"],
      lon: stop["stop_lon"],
      lines: stop["linha"]
    }
  end
end
