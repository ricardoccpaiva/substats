defmodule SubstatsData.Fetcher do
  use GenServer
  alias SubstatsData.Gateways.MetroLx
  alias SubstatsData.Converters

  require Logger

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def start_link(name, args) do
    GenServer.start_link(__MODULE__, args, name: name)
  end

  # GenServer Callbacks
  def init(%{refresh_interval: refresh_interval, fail_refresh_interval: fail_refresh_interval}) do
    Process.send_after(
      self(),
      :update_and_schedule_refresh_access_token,
      0
    )

    Process.send_after(self(), :update_and_schedule, refresh_interval)

    {:ok,
     %{
       access_token: nil,
       access_token_refresh_ttl: 1,
       refresh_interval: refresh_interval,
       fail_refresh_interval: fail_refresh_interval
     }}
  end

  def handle_info({:ssl_closed, _msg}, state), do: {:noreply, state}

  def handle_info(:update_and_schedule, state) do
    if state.access_token != nil do
      refresh_data(state)
    else
      Process.send_after(self(), :update_and_schedule, state.fail_refresh_interval)
    end

    {:noreply, state}
  end

  def handle_info(:update_and_schedule_refresh_access_token, state) do
    Logger.info("----> Refreshing access_token")

    {refresh_interval, new_state} =
      case MetroLx.get_access_token() do
        :error ->
          {state.fail_refresh_interval, state}

        token ->
          new_state =
            Map.replace!(state, :access_token, token[:access_token])
            |> Map.replace!(:access_token_refresh_ttl, token[:ttl])

          {token[:ttl], new_state}
      end

    Process.send_after(
      self(),
      :update_and_schedule_refresh_access_token,
      refresh_interval
    )

    {:noreply, new_state}
  end

  defp refresh_data(state) do
    Logger.info("---> Refreshing data")

    lines = ["Amarela", "Azul", "Vermelha", "Verde"]
    Enum.map(lines, fn line -> update_influx_data(state.access_token, line) end)

    Process.send_after(self(), :update_and_schedule, state.refresh_interval)
  end

  defp update_influx_data(access_token, line) do
    case MetroLx.get_waiting_times(access_token, line) do
      :error ->
        Logger.error("Failed to update influx data")

      waiting_times ->
        Converters.WaitingTime.convert_to_series(waiting_times, line)
        |> SubstatsInfluxDb.write_data()
    end
  end
end
