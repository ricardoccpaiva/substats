defmodule SubstatsData.Gateways.MetroLx do
  require Logger

  def get_access_token() do
    headers = [
      {"Content-Type", "application/x-www-form-urlencoded"},
      {"Accept", "application/json"},
      {"Authorization", Application.get_env(:substats_data, :metro_lx_api_auth_header)}
    ]

    payload = "grant_type=client_credentials"
    url = "https://api.metrolisboa.pt:8243/token"

    case HTTPoison.post(url, payload, headers, hackney: [:insecure]) do
      {:ok, response} ->
        decoded_response = Poison.decode!(response.body)

        %{
          access_token: Map.get(decoded_response, "access_token"),
          ttl: Map.get(decoded_response, "expires_in") * 1000
        }

      error ->
        Logger.error("Failed to get access token: #{inspect(error)}")
        :error
    end
  end

  def get_waiting_times(access_token) do
    headers = [
      {"Accept", "application/json"},
      {"Authorization", "Bearer #{access_token}"}
    ]

    url = "https://api.metrolisboa.pt:8243/estadoServicoML/1.0.1/tempoEspera/Estacao/todos"

    case HTTPoison.get(url, headers, hackney: [:insecure]) do
      {:ok, response} ->
        decoded_response = Poison.decode!(response.body)
        decoded_response["resposta"]

      error ->
        Logger.error("Failed to get waiting times: #{inspect(error)}")
        :error
    end
  end

  def get_waiting_times(access_token, line) do
    headers = [
      {"Accept", "application/json"},
      {"Authorization", "Bearer #{access_token}"}
    ]

    url = "https://api.metrolisboa.pt:8243/estadoServicoML/1.0.1/tempoEspera/Linha/#{line}"

    case HTTPoison.get(url, headers, hackney: [:insecure]) do
      {:ok, response} ->
        decoded_response = Poison.decode!(response.body)
        decoded_response["resposta"]

      error ->
        Logger.error("Failed to get waiting times for line #{line}: #{inspect(error)}")
        :error
    end
  end

  def get_stops_info(access_token) do
    headers = [
      {"Accept", "application/json"},
      {"Authorization", "Bearer #{access_token}"}
    ]

    url = "https://api.metrolisboa.pt:8243/estadoServicoML/1.0.1/infoEstacao/todos"

    case HTTPoison.get(url, headers, hackney: [:insecure]) do
      {:ok, response} ->
        decoded_response = Poison.decode!(response.body)
        {:ok, decoded_response["resposta"]}

      error ->
        Logger.error("Failed to get stops info: #{inspect(error)}")
        :error
    end
  end

  def get_destination_info(access_token) do
    headers = [
      {"Accept", "application/json"},
      {"Authorization", "Bearer #{access_token}"}
    ]

    url = "https://api.metrolisboa.pt:8243/estadoServicoML/1.0.1/infoDestinos/todos"

    case HTTPoison.get(url, headers, hackney: [:insecure]) do
      {:ok, response} ->
        decoded_response = Poison.decode!(response.body)
        {:ok, decoded_response["resposta"]}

      error ->
        Logger.error("Failed to get stops info: #{inspect(error)}")
        :error
    end
  end
end
