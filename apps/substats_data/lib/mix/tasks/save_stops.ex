defmodule Mix.Tasks.SaveStops do
  use Mix.Task
  alias SubstatsData.Gateways.MetroLx
  alias SubstatsData.Interactors.SaveStops
  require Logger

  @impl Mix.Task
  def run(_args) do
    Application.put_env(
      :substats_data,
      :metro_lx_api_auth_header,
      System.get_env("METRO_LX_API_AUTH_HEADER")
    )

    Application.ensure_all_started(:hackney)
    Application.ensure_all_started(:logger)
    Application.ensure_all_started(:substats_repository)

    case MetroLx.get_access_token() do
      :error -> Logger.error("Failed to get access token")
      token -> SaveStops.save_all(token[:access_token])
    end
  end
end
