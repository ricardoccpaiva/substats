defmodule SubstatsData.Converters.WaitingTime do
  alias SubstatsInfluxDb.Models

  def convert_to_series(waiting_times, line) do
    Enum.map(waiting_times, fn wt ->
      %Models.WaitingTime{
        fields: %Models.WaitingTime.Fields{
          waiting_time_n1: get_parsed_value(wt["tempoChegada1"]),
          waiting_time_n2: get_parsed_value(wt["tempoChegada2"]),
          waiting_time_n3: get_parsed_value(wt["tempoChegada3"])
        },
        tags: %Models.WaitingTime.Tags{
          line: line,
          stop_id: wt["stop_id"],
          dock: wt["cais"],
          destination: wt["destino"]
        }
      }
    end)
  end

  defp get_parsed_value(raw_value) do
    {integer_value, ""} = Integer.parse(raw_value)
    integer_value
  end
end
