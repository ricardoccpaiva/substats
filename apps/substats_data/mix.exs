defmodule SubstatsData.MixProject do
  use Mix.Project

  def project do
    [
      app: :substats_data,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison, :ecto_sql, :postgrex],
      mod: {SubstatsData.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.6"},
      {:hackney, "1.16.0", override: true},
      {:poison, "~> 3.1"},
      {:instream, "~> 0.21"},
      {:substats_repository, in_umbrella: true},
      {:substats_influxdb, in_umbrella: true}
    ]
  end
end
