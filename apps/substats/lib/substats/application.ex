defmodule Substats.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the endpoint when the application starts
      SubstatsWeb.Endpoint,
      SubstatsInfluxDb.Connection,
      {Substats.StopsCacher, %{refresh_interval: 2000, fail_refresh_interval: 10000}}

      # Starts a worker by calling: Substats.Worker.start_link(arg)
      # {Substats.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Substats.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SubstatsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
