defmodule Substats.StopsCacher do
  use GenServer
  require Logger

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: :stops_cacher)
  end

  def start_link(name, args) do
    GenServer.start_link(__MODULE__, args, name: name)
  end

  def get_stats(stop_id, timeframe) do
    Process.whereis(:stops_cacher)
    |> GenServer.call({:get_stats, stop_id, timeframe})
  end

  # GenServer Callbacks
  def init(%{refresh_interval: refresh_interval, fail_refresh_interval: fail_refresh_interval}) do
    Process.send_after(self(), :update_and_schedule, refresh_interval)

    {:ok,
     %{
       refresh_interval: refresh_interval,
       fail_refresh_interval: fail_refresh_interval,
       stats: nil
     }}
  end

  def handle_call({:get_stats, stop_id, timeframe}, _from, state) do
    {:reply, get_stats(stop_id, timeframe, state.stats), state}
  end

  def handle_info({:ssl_closed, _msg}, state), do: {:noreply, state}

  def handle_info(:update_and_schedule, state) do
    stats = refresh_data(state)

    Process.send_after(self(), :update_and_schedule, state.refresh_interval)

    SubstatsWeb.Endpoint.broadcast("waiting_times:all", "updated_data", %{stats: stats})

    {:noreply, %{state | stats: stats}}
  end

  defp refresh_data(state) do
    Logger.info("---> Refreshing data")

    ["5m", "15m", "30m", "60m", "12h", "24h", "168h", "720h"]
    |> Enum.map(&%{id: &1, data: Task.async(fn -> SubstatsInfluxDb.get_mean(&1) end)})
    |> Enum.map(&%{id: &1.id, data: Task.await(&1.data)})
    |> Enum.map(&build_result(&1, state))
  end

  defp build_result(results, state) do
    case results.data do
      {:error, _reason} ->
        case state.stats do
          nil ->
            %{id: results.id, last_updated: nil, data: nil}

          current_state_stats ->
            current_timeframe_stats =
              Enum.find(current_state_stats, fn elem -> elem[:id] == results.id end)

            %{
              id: results.id,
              last_updated: Map.get(current_timeframe_stats, :last_updated),
              data: Map.get(current_timeframe_stats, :data)
            }
        end

      nil ->
        %{id: results.id, last_updated: nil, data: nil}

      data ->
        %{
          id: results.id,
          last_updated: DateTime.utc_now(),
          data: reduce_data(data)
        }
    end
  end

  defp reduce_data(data) do
    case data do
      nil ->
        nil

      _ ->
        Enum.reduce(data[:results], [], fn elem_resuls, _acc_results ->
          Enum.reduce(elem_resuls[:series], [], fn elem_series, acc_series ->
            new = %{
              stop_id: elem_series[:tags][:stop_id],
              destination: elem_series[:tags][:destination],
              waiting_times: elem_series[:values] |> List.flatten() |> List.delete_at(0)
            }

            [new | acc_series]
          end)
        end)
    end
  end

  defp get_stats(stop_id, timeframe, data) do
    data
    |> Enum.find(fn e -> e.id == timeframe end)
    |> Map.get(:data)
    |> Enum.find(fn e -> e.stop_id == stop_id end)
  end
end
