defmodule Substats.Interactors.GetStops do
  alias SubstatsRepository.Models.Stop
  import Ecto.Query

  def call() do
    from(stop in Stop, select: stop)
    |> SubstatsRepository.Repo.all()
    |> Enum.map(fn stop ->
      %{
        id: stop.external_id,
        internal_id: stop.internal_id,
        name: stop.name,
        lat: stop.lat,
        lon: stop.lon
      }
    end)
  end
end
