defmodule SubstatsWeb.StopsController do
  use SubstatsWeb, :controller
  alias Substats.Interactors.GetStops

  def get_all(conn, _params) do
    ret = GetStops.call()

    json(conn, ret)
  end
end
