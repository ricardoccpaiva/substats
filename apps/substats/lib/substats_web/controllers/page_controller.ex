defmodule SubstatsWeb.PageController do
  use SubstatsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
