defmodule SubstatsWeb.WaitingTimesChannel do
  use Phoenix.Channel

  def join("waiting_times:all", _message, socket) do
    {:ok, socket}
  end

  def join(_, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end
end
