defmodule SubstatsWeb.Router do
  use SubstatsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SubstatsWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", SubstatsWeb do
    pipe_through(:api)

    get("/stops", StopsController, :get_all)
  end

  # Other scopes may use custom stacks.
  # scope "/api", SubstatsWeb do
  #   pipe_through :api
  # end
end
