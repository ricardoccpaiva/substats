defmodule SubstatsInfluxDb do
  def write_data(items) do
    SubstatsInfluxDb.Connection.write(items)
  end

  def get_mean(timeframe, stop_id) do
    "select mean(waiting_time_n1) as mean_n1, mean(waiting_time_n2) as mean_n2, mean(waiting_time_n3) as mean_n3 \
    from waiting_time where time > now() - #{timeframe} and stop_id = '#{stop_id}'"
    |> SubstatsInfluxDb.Connection.query(database: "substats")
  end

  def get_mean(timeframe) do
    "select mean(waiting_time_n1) as mean_n1, mean(waiting_time_n2) as mean_n2, mean(waiting_time_n3) as mean_n3 \
    from waiting_time where time > now() - #{timeframe} \
    group by stop_id, destination"
    |> SubstatsInfluxDb.Connection.query(database: "substats")
  end

end
