defmodule SubstatsInfluxDb.Models.WaitingTime do
  use Instream.Series

  series do
    database("substats")
    measurement("waiting_time")

    tag(:line)
    tag(:stop_id)
    tag(:dock)
    tag(:destination)

    field(:waiting_time_n1)
    field(:waiting_time_n2)
    field(:waiting_time_n3)
  end
end
