defmodule SubstatsData.Repository.Migrations.AddStopId do
  use Ecto.Migration

  def change do
    alter table("stop") do
      add(:internal_id, :string, size: 10)
    end
  end
end
