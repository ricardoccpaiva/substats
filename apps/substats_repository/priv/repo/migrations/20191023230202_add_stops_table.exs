defmodule SubstatsData.Repository.Migrations.AddStopsTable do
  use Ecto.Migration

  def change do
    create table("stop") do
      add :external_id, :string, size: 10
      add :name, :string, size: 50
      add :lat, :string, size: 25
      add :lon, :string, size: 25
      add :lines, :string, size: 250
      timestamps()
    end
  end
end
