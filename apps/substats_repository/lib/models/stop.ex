defmodule SubstatsRepository.Models.Stop do
  use Ecto.Schema
  import Ecto.Changeset

  schema "stop" do
    # Defaults to type :string
    field(:external_id)
    field(:internal_id)
    field(:name)
    field(:lat)
    field(:lon)
    field(:lines)

    timestamps()
  end

  def changeset(stop, params \\ %{}) do
    stop
    |> cast(params, [:external_id, :internal_id, :name, :lat, :lon, :lines])
    |> validate_required([:external_id, :internal_id, :name, :lat, :lon, :lines])
    |> unique_constraint(:external_id)
  end
end
