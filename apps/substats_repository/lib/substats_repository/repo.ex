defmodule SubstatsRepository.Repo do
  use Ecto.Repo,
    otp_app: :substats_repository,
    adapter: Ecto.Adapters.Postgres
end
