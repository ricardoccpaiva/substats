use Mix.Config

config :substats_repository, ecto_repos: [SubstatsRepository.Repo]

import_config "#{Mix.env()}.exs"
