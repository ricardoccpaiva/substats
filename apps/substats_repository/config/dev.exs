use Mix.Config

config :substats_repository, SubstatsRepository.Repo,
  database: "substats",
  username: "postgres",
  password: System.get_env("PG_PASS"),
  hostname: System.get_env("PG_HOST"),
  port: "5432",
  show_sensitive_data_on_connection_error: true
